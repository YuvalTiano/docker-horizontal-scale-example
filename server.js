'use strict';

const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch');

const PORT = 8080;
const HOST = '0.0.0.0';
const DNS_ALIAS = process.env.DNS_ALIAS || HOST;

// Initial Server:
const app = express();
app.use(cors())

// Health Check:
app.get('/', (req, res) => {
  res.json({
    message: `My name is ${DNS_ALIAS}`,
  });
});

// Make a call to service `dns_name`:
app.get('/talk/:dns_name', (req, res) => {
  fetch(`http://${req.params.dns_name}:${PORT}`, {
    method: "POST",
  })
    .then(serviceResponse => serviceResponse.json())
    .then((serviceMessage) => res.json(serviceMessage))
    .catch((error) => {
      switch (error.code) {
        case 'ENOTFOUND':
          return res.json({ message: `${req.params.dns_name} is not found` })
        default:
          return res.json({ message: 'An error is happened :(' })
      }
    });
});

// Handle the request from the service:
app.post('/', (req, res) => {
  res.json({
    message: `My name is ${DNS_ALIAS} and my address is http://${HOST}:${PORT}`,
  });
});

app.listen(PORT, HOST, () => {
  console.info(`Server is running http://${HOST}:${PORT}`)
});
